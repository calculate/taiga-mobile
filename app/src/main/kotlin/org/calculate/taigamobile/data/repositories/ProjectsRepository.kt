package org.calculate.taigamobile.data.repositories

import org.calculate.taigamobile.data.api.TaigaApi
import org.calculate.taigamobile.domain.paging.CommonPagingSource
import org.calculate.taigamobile.domain.repositories.IProjectsRepository
import org.calculate.taigamobile.state.Session
import javax.inject.Inject

class ProjectsRepository @Inject constructor(
    private val taigaApi: TaigaApi,
    private val session: Session
) : IProjectsRepository {

    override suspend fun searchProjects(query: String, page: Int) = withIO {
        handle404 {
            taigaApi.getProjects(
                query = query,
                page = page,
                pageSize = CommonPagingSource.PAGE_SIZE
            )
        }
    }

    override suspend fun getMyProjects() = withIO {
        taigaApi.getProjects(memberId = session.currentUserId.value)
    }

    override suspend fun getUserProjects(userId: Long) = withIO {
        taigaApi.getProjects(memberId = userId)
    }
}