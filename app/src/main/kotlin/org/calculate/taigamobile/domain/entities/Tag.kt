package org.calculate.taigamobile.domain.entities

data class Tag(
    val name: String,
    val color: String
)
