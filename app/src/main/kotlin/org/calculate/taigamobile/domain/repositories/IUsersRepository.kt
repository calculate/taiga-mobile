package org.calculate.taigamobile.domain.repositories

import org.calculate.taigamobile.domain.entities.Stats
import org.calculate.taigamobile.domain.entities.TeamMember
import org.calculate.taigamobile.domain.entities.User

interface IUsersRepository {
    suspend fun getMe(): User
    suspend fun getUser(userId: Long): User
    suspend fun getUserStats(userId: Long): Stats
    suspend fun getTeam(): List<TeamMember>
}