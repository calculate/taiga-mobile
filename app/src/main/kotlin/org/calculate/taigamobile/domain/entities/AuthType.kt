package org.calculate.taigamobile.domain.entities

enum class AuthType {
    Normal,
    LDAP
}