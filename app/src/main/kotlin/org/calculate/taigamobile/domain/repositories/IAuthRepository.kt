package org.calculate.taigamobile.domain.repositories

import org.calculate.taigamobile.domain.entities.AuthType

interface IAuthRepository {
    suspend fun auth(taigaServer: String, authType: AuthType, password: String, username: String)
}