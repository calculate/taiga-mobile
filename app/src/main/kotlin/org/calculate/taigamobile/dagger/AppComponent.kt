package org.calculate.taigamobile.dagger

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import org.calculate.taigamobile.ui.screens.login.LoginViewModel
import org.calculate.taigamobile.ui.screens.main.MainViewModel
import org.calculate.taigamobile.ui.screens.projectselector.ProjectSelectorViewModel
import org.calculate.taigamobile.ui.screens.scrum.ScrumViewModel
import org.calculate.taigamobile.ui.screens.sprint.SprintViewModel
import org.calculate.taigamobile.ui.screens.commontask.CommonTaskViewModel
import org.calculate.taigamobile.ui.screens.createtask.CreateTaskViewModel
import org.calculate.taigamobile.ui.screens.dashboard.DashboardViewModel
import org.calculate.taigamobile.ui.screens.epics.EpicsViewModel
import org.calculate.taigamobile.ui.screens.issues.IssuesViewModel
import org.calculate.taigamobile.ui.screens.kanban.KanbanViewModel
import org.calculate.taigamobile.ui.screens.profile.ProfileViewModel
import org.calculate.taigamobile.ui.screens.settings.SettingsViewModel
import org.calculate.taigamobile.ui.screens.team.TeamViewModel
import org.calculate.taigamobile.ui.screens.wiki.createpage.WikiCreatePageViewModel
import org.calculate.taigamobile.ui.screens.wiki.page.WikiPageViewModel
import org.calculate.taigamobile.ui.screens.wiki.list.WikiListViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [DataModule::class, RepositoriesModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun context(context: Context): Builder
        fun build(): AppComponent
    }

    fun inject(mainViewModel: MainViewModel)
    fun inject(loginViewModel: LoginViewModel)
    fun inject(dashboardViewModel: DashboardViewModel)
    fun inject(scrumViewModel: ScrumViewModel)
    fun inject(epicsViewModel: EpicsViewModel)
    fun inject(projectSelectorViewModel: ProjectSelectorViewModel)
    fun inject(sprintViewModel: SprintViewModel)
    fun inject(commonTaskViewModel: CommonTaskViewModel)
    fun inject(teamViewModel: TeamViewModel)
    fun inject(settingsViewModel: SettingsViewModel)
    fun inject(createTaskViewModel: CreateTaskViewModel)
    fun inject(issuesViewModel: IssuesViewModel)
    fun inject(kanbanViewModel: KanbanViewModel)
    fun inject(profileViewModel: ProfileViewModel)
    fun inject(wikiSelectorViewModel: WikiListViewModel)
    fun inject(wikiPageViewModel: WikiPageViewModel)
    fun inject(wikiCreatePageViewModel: WikiCreatePageViewModel)
}