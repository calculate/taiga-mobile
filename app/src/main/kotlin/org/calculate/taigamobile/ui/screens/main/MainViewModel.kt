package org.calculate.taigamobile.ui.screens.main

import androidx.lifecycle.*
import org.calculate.taigamobile.state.Session
import org.calculate.taigamobile.state.Settings
import org.calculate.taigamobile.TaigaApp
import org.calculate.taigamobile.dagger.AppComponent
import javax.inject.Inject

class MainViewModel(appComponent: AppComponent = TaigaApp.appComponent) : ViewModel() {
    @Inject lateinit var session: Session
    @Inject lateinit var settings: Settings

    val isLogged by lazy { session.isLogged }
    val isProjectSelected by lazy { session.isProjectSelected }

    val theme by lazy { settings.themeSetting }

    init {
        appComponent.inject(this)
    }
}
