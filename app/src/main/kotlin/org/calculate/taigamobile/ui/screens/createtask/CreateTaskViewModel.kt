package org.calculate.taigamobile.ui.screens.createtask

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.calculate.taigamobile.TaigaApp
import org.calculate.taigamobile.dagger.AppComponent
import org.calculate.taigamobile.domain.entities.CommonTask
import org.calculate.taigamobile.domain.entities.CommonTaskType
import org.calculate.taigamobile.domain.repositories.ITasksRepository
import org.calculate.taigamobile.state.Session
import org.calculate.taigamobile.state.postUpdate
import org.calculate.taigamobile.ui.utils.MutableResultFlow
import org.calculate.taigamobile.ui.utils.loadOrError
import kotlinx.coroutines.launch
import javax.inject.Inject

class CreateTaskViewModel(appComponent: AppComponent = TaigaApp.appComponent) : ViewModel() {
    @Inject lateinit var tasksRepository: ITasksRepository
    @Inject lateinit var session: Session

    init {
        appComponent.inject(this)
    }

    val creationResult = MutableResultFlow<CommonTask>()

    fun createTask(
        commonTaskType: CommonTaskType,
        title: String,
        description: String,
        parentId: Long? = null,
        sprintId: Long? = null,
        statusId: Long? = null,
        swimlaneId: Long? = null
    ) = viewModelScope.launch {
        creationResult.loadOrError(preserveValue = false) {
            tasksRepository.createCommonTask(commonTaskType, title, description, parentId, sprintId, statusId, swimlaneId).also {
                session.taskEdit.postUpdate()
            }
        }
    }
}