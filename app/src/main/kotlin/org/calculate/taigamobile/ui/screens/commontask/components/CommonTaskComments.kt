package org.calculate.taigamobile.ui.screens.commontask.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import org.calculate.taigamobile.R
import org.calculate.taigamobile.domain.entities.Comment
import org.calculate.taigamobile.ui.components.dialogs.ConfirmActionDialog
import org.calculate.taigamobile.ui.components.lists.UserItem
import org.calculate.taigamobile.ui.components.loaders.DotsLoader
import org.calculate.taigamobile.ui.components.texts.MarkdownText
import org.calculate.taigamobile.ui.components.texts.SectionTitle
import org.calculate.taigamobile.ui.screens.commontask.EditActions

@Suppress("FunctionName")
fun LazyListScope.CommonTaskComments(
    comments: List<Comment>,
    editActions: EditActions,
    navigateToProfile: (userId: Long) -> Unit
) {
    item {
        SectionTitle(stringResource(R.string.comments_template).format(comments.size))
    }

    itemsIndexed(comments) { index, item ->
        CommentItem(
            comment = item,
            onDeleteClick = { editActions.editComments.remove(item) },
            navigateToProfile = navigateToProfile
        )

        if (index < comments.lastIndex) {
            Divider(
                modifier = Modifier.padding(vertical = 12.dp),
                color = MaterialTheme.colorScheme.outline
            )
        }
    }

    item {
        if (editActions.editComments.isLoading) {
            DotsLoader()
        }
    }
}

@Composable
private fun CommentItem(
    comment: Comment,
    onDeleteClick: () -> Unit,
    navigateToProfile: (userId: Long) -> Unit
) = Column {
    var isAlertVisible by remember { mutableStateOf(false) }

    if (isAlertVisible) {
        ConfirmActionDialog(
            title = stringResource(R.string.delete_comment_title),
            text = stringResource(R.string.delete_comment_text),
            onConfirm = {
                isAlertVisible = false
                onDeleteClick()
            },
            onDismiss = { isAlertVisible = false },
            iconId = R.drawable.ic_delete
        )
    }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        UserItem(
            user = comment.author,
            dateTime = comment.postDateTime,
            onUserItemClick = { navigateToProfile(comment.author.id) }
        )

        if (comment.canDelete) {
            IconButton(onClick = { isAlertVisible = true }) {
                Icon(
                    painter = painterResource(R.drawable.ic_delete),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.error
                )
            }
        }
    }

    MarkdownText(
        text = comment.text,
        modifier = Modifier.padding(start = 4.dp)
    )
}
