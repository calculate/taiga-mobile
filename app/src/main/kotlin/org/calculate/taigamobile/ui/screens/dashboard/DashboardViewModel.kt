package org.calculate.taigamobile.ui.screens.dashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.calculate.taigamobile.state.Session
import org.calculate.taigamobile.TaigaApp
import org.calculate.taigamobile.dagger.AppComponent
import org.calculate.taigamobile.domain.entities.CommonTask
import org.calculate.taigamobile.domain.entities.Project
import org.calculate.taigamobile.domain.repositories.IProjectsRepository
import org.calculate.taigamobile.domain.repositories.ITasksRepository
import org.calculate.taigamobile.ui.utils.MutableResultFlow
import org.calculate.taigamobile.ui.utils.NothingResult
import org.calculate.taigamobile.ui.utils.loadOrError
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import javax.inject.Inject

class DashboardViewModel(appComponent: AppComponent = TaigaApp.appComponent) : ViewModel() {
    @Inject lateinit var tasksRepository: ITasksRepository
    @Inject lateinit var projectsRepository: IProjectsRepository
    @Inject lateinit var session: Session

    val workingOn = MutableResultFlow<List<CommonTask>>()
    val watching = MutableResultFlow<List<CommonTask>>()
    val myProjects = MutableResultFlow<List<Project>>()

    val currentProjectId by lazy { session.currentProjectId }

    private var shouldReload = true

    init {
        appComponent.inject(this)
    }

    fun onOpen() = viewModelScope.launch {
        if (!shouldReload) return@launch
        joinAll(
            launch { workingOn.loadOrError(preserveValue = false) { tasksRepository.getWorkingOn() } },
            launch { watching.loadOrError(preserveValue = false) { tasksRepository.getWatching() } },
            launch { myProjects.loadOrError(preserveValue = false) { projectsRepository.getMyProjects() } }
        )
        shouldReload = false
    }

    fun changeCurrentProject(project: Project) {
        project.apply {
            session.changeCurrentProject(id, name)
        }
    }

    init {
        session.taskEdit.onEach {
            workingOn.value = NothingResult()
            watching.value = NothingResult()
            myProjects.value = NothingResult()
            shouldReload = true
        }.launchIn(viewModelScope)
    }
}
