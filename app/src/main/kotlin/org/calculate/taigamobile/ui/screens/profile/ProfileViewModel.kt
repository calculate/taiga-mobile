package org.calculate.taigamobile.ui.screens.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.calculate.taigamobile.TaigaApp
import org.calculate.taigamobile.dagger.AppComponent
import org.calculate.taigamobile.domain.entities.Project
import org.calculate.taigamobile.domain.entities.Stats
import org.calculate.taigamobile.domain.entities.User
import org.calculate.taigamobile.domain.repositories.IProjectsRepository
import org.calculate.taigamobile.domain.repositories.IUsersRepository
import org.calculate.taigamobile.state.Session
import org.calculate.taigamobile.ui.utils.MutableResultFlow
import org.calculate.taigamobile.ui.utils.loadOrError
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileViewModel(appComponent: AppComponent = TaigaApp.appComponent) : ViewModel() {
    @Inject
    lateinit var usersRepository: IUsersRepository

    @Inject
    lateinit var projectsRepository: IProjectsRepository

    @Inject
    lateinit var session: Session

    val currentUser = MutableResultFlow<User>()
    val currentUserStats = MutableResultFlow<Stats>()
    val currentUserProjects = MutableResultFlow<List<Project>>()
    val currentProjectId by lazy { session.currentProjectId }

    init {
        appComponent.inject(this)
    }

    fun onOpen(userId: Long) = viewModelScope.launch {
        currentUser.loadOrError { usersRepository.getUser(userId) }
        currentUserStats.loadOrError { usersRepository.getUserStats(userId) }
        currentUserProjects.loadOrError { projectsRepository.getUserProjects(userId) }
    }
}