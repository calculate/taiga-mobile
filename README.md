# Введение

Это форк неофициального приложения [TaigaMobile](https://github.com/EugeneTheDev/TaigaMobile) написанного для Android для гибкой системы управления проектами [Taiga.io](https://www.taiga.io/). Приложение скомпилированно и собрано в интегрированной среде разработки (IDE) Android Studio.

# Сборка

1. Установите Android Studio:  
`emerge -av dev-util/android-studio`

2. Клонируйте репозиторий:  
`git clone https://git.calculate-linux.org/calculate/taiga-mobile.git`

3. Создайте в дериктории 'app' пустой файл signing.properties в нем будет храниться пароль от ключа:  
`touch app/signing.properties`

4. Запустите Android Studio и откройте проект:  
"Projects -> Open -> ../TaigaMobile -> Trust project".  

5. Обновите компилятор Gradle до последней версии (обновляется автоматически при первом открытии проекта).  

6. Скомпилировать и собрать проект:  
"Build -> Make Project".

7. Собрать Apk с новой подписью и новым именем:  
"Build -> Build Bundle(s)/APK(s) -> Build APK(s)".

# Кастомизация

## Изменение логотипа приложения

1. Замените логотип "app/src/main/ic_launcher-playstore.png" на "app/src/main/ваш_логотип.png".

2. Замените логотипы **всех размеров**, находящиеся в "app/src/main/res":
`rm app/src/main/res/mipmap-hdpi/*`    
`convert {app/src/main/ваш_логотип.png} -resize {size} {app/src/main/res/mipmap-hdpi/ваш_логотип.png}`  

Размеры:
		    
* mipmap-hdpi		- 48  
* mipmap-mdpi		- 72  
* mipmap-xhdpi		- 96  
* mipmap-xxhdpi		- 144  
* mipmap-xxxhdpi	- 192  

3. В Android Studio откройте для редактирования файл "app/manifests/AndroidManifest.xml".

4. Замените "@mipmap/ic_launcher" и "@mipmap/ic_launcher_round" на "@mipmap/ваш_логотип".

## Изменение имени пакета

1. Замените имена каталогов "../io/eugenethedev/taigamobile" на "../ваше/новоеимя/taigamobile":  
`find . \( -type d -a -name 'io' \) | sort -r | while read d;do mv "$d" "${d/%io/ваше}";done`  
`find . \( -type d -a -name 'eugenethedev' \) | sort -r | while read d;do mv "$d" "${d/%eugenethedev/новоеимя}";done`  

2. Внутри всех исходных кодов заменить пути "io.eugenethedev.taigamobile" на "ваше.новоеимя.taigamobile":  
`files=$(grep -rl io.eugenethedev app/);for f in ${files[@]};do sed -i s/io.eugenethedev/ваше.новоеимя/g $f;done`  

3. В файле "app/build.gradle.kts" заменить namespace = "io.eugenethedev.taigamobile" на namespace = "ваше.новоеимя.taigamobile".

## Изменение подписи проекта

1. Устанавите Java:  
`emerge -av dev-lang/gnuprologjava`

2. Очистите каталог app/keystores и сгенерируйте новые ключи:  
`rm -R app/keystores/*`  
`keytool -genkey -v -keystore debug.keystore -alias debug -keyalg RSA -keysize 2048 -validity 10000` (с паролем "android"!)  
`keytool -genkey -v -keystore release.keystore -alias taiga-release -keyalg RSA -keysize 2048 -validity 10000`

3. Запишите alias и пароль от "release.keystore" в файл "signing.properties" форматом:  
password="$PASSWORD"  
alias="taiga-release"  
